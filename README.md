# Anthony Sandoval

In the last couple years, the trend of writing a manager README emerged. I've read a number of them, and both the favorable arguments and critiques<sup><a href='ref1'>1</a></sup>. Most are generally helpful, some are amusing, and all appear well intentioned. I've determined that writing mine adds value because of the asynchronous and remote nature of our team. This is not a contract. It's a place for information that I believe will help keep you, me, and our team well organized.

## Communication

If you haven't already, please read the [GitLab Communication](https://about.gitlab.com/handbook/communication/) page in the handbook. Then, wait a week and reread it. Wait a month, and reread it again.

Working at GitLab is unlike working anywhere else. Firstly, because we use GitLab the product as our primary means of communicating our ideas and opinions. GitLab Todos can be overwhelming from day one. Developing your own workflow for organizing issues–separating out issues that require your action or attention–is a tricky endeavor. You'll need to find what works for you.

No, _really_, you need to continually hone your workflow. The company communicates in GitLab the product and you must learn to participate effectively. This means keeping track of issues and responding when you're at-mentioned or assigned.

A few **pro-tips**:

* You can search issues and MRs based on your emoji responses. This may or not be useful for you, but it's pretty cool to have your files of 👀❗️👍🙏 issues to open.
* Learn how the [notifications](https://docs.gitlab.com/ee/workflow/notifications.html) work and tune them.
* If you're not a Gmail power user, learn about the great [search operators](https://support.google.com/mail/answer/7190?hl=en) you can use to set filters.
* Lastly, think of Todos more like notifications with actions attached to them: read me, comment on me, close me, etc. If you think of Todos as an organizational tool for long-lived tasks, then the number of Todos is likely to stack high and become unmanageable.

### Slack

I neither like nor dislike Slack. It does some things really well, but if mistreated it's a distraction and productivity killer. So, I treat it like a feed of information–a log of mixed human and bot activity. If I show offline or don't respond in Slack, it doesn't necessarily mean I'm not available. If you want a guaranteed response, email me. If you need me immediately, page me.

#### Geekbot

Do your best to respond to Geekbot daily. In addition to the log in Slack, I receive an email summary daily. If you're blocked on something this is a great way to gain exposure on the topic and it's email, so you can be certain I'll see it.

## Meetings

### One-on-Ones

I attempt to keep a weekly cadence to one-on-one meetings with direct reports. You can expect to receive a meeting invite from me at the start of each week. I recently abandoned using a recurring calendar invite. Experience has taught me that the recurring schedule and rolling agenda encouraged postponing urgent discussions until the next meeting. It's important to me that when you need me I'm available. So, I keep blocks of [appointment slots](https://calendar.google.com/calendar/selfsched?sstoken=UU5Gai1hQUp5NW16fGRlZmF1bHR8MzQ1ZTY2OTc5N2I4YWFlMDg2MTM5ZTk2N2ExNmUzZGE) on my calendar–use them if you need them! But, if it can't wait and you need me, send an invite!

If you'd like, I can send a recurring invite. But, rest assured you're not likely to go longer than a week without seeing a calendar invite from me. Don't be surprised–or alarmed! 

## Focal Points

There are a number of things I'd like to see run smoother for our team–and our department–that are currently suboptimal in their design, execution, or both. I'm focused on bringing attention and improvements to the following.

### Incident Management

Reliability Engineering is still maturing and we're in a unique position where we have influence over the product vision–unless it's unavailable, we can triage our issues with GitLab.com.

https://gitlab.com/groups/gitlab-org/-/epics/349
https://about.gitlab.com/handbook/engineering/infrastructure/incident-management/index.html

### Career Development

The company is making great progress maturing our [Engineering Career Development](https://about.gitlab.com/handbook/engineering/career-development/) process. 

### Diversity Hiring

[Diversity](https://about.gitlab.com/handbook/values/#diversity) is one of our core values. I believe that the inclusion of diverse viewpoints and experiences improves the way we work and ultimately builds better tools.


<hr>
References<br>

<sub>
<a name="ref1">1.</a> <a href="https://medium.com/@skamille/i-hate-manager-readmes-20a0dd9a70d0">I hate manager READMEs</a>, Camile Fournier
</sub>
